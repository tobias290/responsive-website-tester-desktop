import Vue from "vue";
import App from "./components/App.vue";

Vue.config.productionTip = false;

if (window.dataRemote === undefined) {
    let defaultDisplayData = [
        {
            "_id": "JDxLq1YRXj",
            "type": "mobile",
            "name": "iPhone XR",
            "height": 896,
            "width": 414,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "8fi5bGbQYm",
            "type": "mobile",
            "name": "iPhone XS",
            "height": 812,
            "width": 375,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "L69dLfJ0Qf",
            "type": "mobile",
            "name": "iPhone XS Max",
            "height": 896,
            "width": 414,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "25Mz1w7liQ",
            "type": "mobile",
            "name": "iPhone X",
            "height": 812,
            "width": 375,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "FWDf4iM5bm",
            "type": "mobile",
            "name": "iPhone 6/7/8 Plus",
            "height": 736,
            "width": 414,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "Yua254hx29",
            "type": "mobile",
            "name": "iPhone 6/7/8",
            "height": 667,
            "width": 375,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "hm9L9N6iq8",
            "type": "mobile",
            "name": "iPhone 5",
            "height": 568,
            "width": 320,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "04NbfXZ3rl",
            "type": "mobile",
            "name": "Nexus 5X/6P",
            "height": 732,
            "width": 412,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "1LFsY17jzp",
            "type": "mobile",
            "name": "Google Pixel 3 XL",
            "height": 847,
            "width": 412,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "pymCbGrEzh",
            "type": "mobile",
            "name": "Google Pixel 3",
            "height": 824,
            "width": 412,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "qzgGwy1qSH",
            "type": "mobile",
            "name": "Google Pixel 1/1XL/2XL",
            "height": 732,
            "width": 412,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "MpD5j3LOwX",
            "type": "mobile",
            "name": "Samsung Galaxy Note 5/One Plus 3",
            "height": 853,
            "width": 480,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "92ktYWYLFF",
            "type": "mobile",
            "name": "Samsung Galaxy S8/S8+/S9/S9+/Note 9",
            "height": 740,
            "width": 360,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },
        {
            "_id": "7sE5ts1633",
            "type": "mobile",
            "name": "Samsung Galaxy S7/S7 Edge",
            "height": 640,
            "width": 360,
            "icon": "fas fa-mobile-alt",
            "custom": false
        },

        {
            "_id": "D1clDytagz",
            "type": "tablet",
            "name": "iPad Pro",
            "height": 1366,
            "width": 1024,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },
        {
            "_id": "W9N7NVOCrr",
            "type": "tablet",
            "name": "iPad",
            "height": 1024,
            "width": 768,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },
        {
            "_id": "jkGEtmfLTB",
            "type": "tablet",
            "name": "Nexus 9",
            "height": 1024,
            "width": 768,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },
        {
            "_id": "7AbzIsE29y",
            "type": "tablet",
            "name": "Nexus 7 (2013)",
            "height": 960,
            "width": 600,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },
        {
            "_id": "xWzKZXOsVz",
            "type": "tablet",
            "name": "Samsung Galaxy Tab 10",
            "height": 1280,
            "width": 800,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },
        {
            "_id": "jjhwwj46Iu",
            "type": "tablet",
            "name": "Chromebook Pixel",
            "height": 850,
            "width": 1280,
            "icon": "fas fa-tablet-alt",
            "custom": false
        },

        {
            "_id": "rvGZHlifSo",
            "type": "laptop",
            "name": "Laptop Small",
            "height": 768,
            "width": 1366,
            "icon": "fas fa-laptop",
            "custom": false
        },
        {
            "_id": "8KPa8Bljha",
            "type": "laptop",
            "name": "Laptop Large",
            "height": 900,
            "width": 1600,
            "icon": "fas fa-laptop",
            "custom": false
        },

        {
            "_id": "K21uRTjC6J",
            "type": "desktop",
            "name": "Desktop Small",
            "height": 1050,
            "width": 1680,
            "icon": "fas fa-desktop",
            "custom": false
        },
        {
            "_id": "ftjYQPrsFQ",
            "type": "desktop",
            "name": "Desktop Large",
            "height": 1080,
            "width": 1920,
            "icon": "fas fa-desktop",
            "custom": false
        }
    ];

    if (!Object.prototype.hasOwnProperty.call(localStorage, "displayTypes")) {
        localStorage.setItem("displayTypes", JSON.stringify(defaultDisplayData));
    }

    if (!Object.prototype.hasOwnProperty.call(localStorage, "displays")) {
        localStorage.setItem("displays", JSON.stringify([]));
    }

    window.dataRemote = class DataRemote {
        constructor(key) {
            this.key = key;
        }

        static get displayTypesDb() {
            return new DataRemote("displayTypes");
        }

        static get displaysDb() {
            return new DataRemote("displays");
        }

        __generateNewId(existingIds) {
            let id = "";
            let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            let validId = false;

            while (!validId) {
                for (let i = 0; i < 10; i++)
                    id += characters.charAt(Math.floor(Math.random() * characters.length));

                validId = !existingIds.includes(id);
            }

            return id;
        }

        findAll() {
            return new Promise((resolve, reject) => {
                if (Object.prototype.hasOwnProperty.call(localStorage, this.key)) {
                    resolve(JSON.parse(localStorage.getItem(this.key)));
                } else {
                    reject(new Error("No display types"));
                }
            });
        }

        insert(data) {
            return new Promise((resolve, reject) => {
                if (Object.prototype.hasOwnProperty.call(localStorage, this.key)) {
                    let dataSoFar = JSON.parse(localStorage.getItem(this.key));

                    let ids = [];

                    for (let datum of dataSoFar)
                        ids.push(datum._id);

                    data._id = this.__generateNewId(ids);

                    dataSoFar.push(data);

                    localStorage.setItem(this.key, JSON.stringify(dataSoFar));

                    resolve(data);
                } else {
                    reject(new Error("No display types"));
                }
            });
        }

        remove(id) {
            return new Promise((resolve, reject) => {
                if (Object.prototype.hasOwnProperty.call(localStorage, this.key)) {
                    let dataSoFar = JSON.parse(localStorage.getItem(this.key));

                    for (let index in dataSoFar) {
                        if (dataSoFar[index]._id === id) {
                            dataSoFar.splice(id, 1);
                            break;
                        }
                    }

                    localStorage.setItem(this.key, JSON.stringify(dataSoFar));

                    resolve(1);
                } else {
                    reject(new Error("No display types"));
                }
            });
        }
    };

    window.isDesktop = false;
}

new Vue({
    render: h => h(App),
}).$mount("#app");

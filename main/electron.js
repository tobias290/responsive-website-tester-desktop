const electron = require("electron");
const app = electron.app;
const path = require("path");
const url = require("url");

let mainWindow;

function createWindow() {
    mainWindow = new electron.BrowserWindow({
        width: 1500,
        height: 1000,
        minHeight: 800,
        minWidth: 700,
        frame: false,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
            //webSecurity: false
        },
    });

    // Build Path: path.join(__dirname, "../public/build/index.html")
    // Dev path: "http://localhost:8081"
    mainWindow.loadURL(url.format({
        protocol: "file",
        slashes: true,
        pathname: path.join(__dirname, "../public/build/index.html")
    }));

    mainWindow.webContents.openDevTools();

    mainWindow.on("closed", () => {
        mainWindow.removeAllListeners();
        mainWindow = null;
    });
}

app.on("ready", () => {
    require("./remotes/DataRemote").connect(
        path.join(app.getPath("userData"), "displayTypes.db"),
        path.join(app.getPath("userData"), "displays.db")
    );
    createWindow();
});

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});

process.on("uncaughtException", (err) => {
    console.log("ERROR!");

    let logger = require("logger").createLogger("errors.log");

    logger.setLevel("error");
    logger.error(err);
});

const WindowRemote = require("./remotes/WindowRemote");
const DataRemote = require("./remotes/DataRemote");

window.windowRemote = WindowRemote;
window.dataRemote = DataRemote;
window.isDesktop = true;

const Remote = require("./Remote");

/**
 * Class to access properties and methods of the applications window.
 */
class WindowRemote extends Remote {
    /**
     * @private
     *
     * Application 'BrowserWindow' instance.
     *
     * @type {Electron.BrowserWindow}
     */
    static window = WindowRemote.remote.BrowserWindow.getFocusedWindow();

    /**
     * @public
     *
     * Minimises the application.
     */
    static minimise() {
        WindowRemote.window.minimize();
    }

    /**
     * @public
     *
     * Either maximises the window or restores it.
     */
    static maximiseOrRestore() {
        WindowRemote.window.isMaximized() ? WindowRemote.window.restore() : WindowRemote.window.maximize();
    }

    /**
     * @public
     *
     * Returns true is the window is maximized, false if not,
     */
    static isMaximised() {
        return WindowRemote.window.isMaximized()
    }

    /**
     * @public
     *
     * Closes the application.
     */
    static close() {
        WindowRemote.window.close();
    }

    /**
     * @public
     *
     * Quits the application.
     */
    static quit() {
        WindowRemote.remote.app.quit();
    }

    /**
     * @public
     *
     * @returns {number} Returns the windows width.
     */
    static getWidth() {
        return WindowRemote.window.getSize()[0];
    }

    /**
     * @public
     *
     * @returns {number} Returns the windows height.
     */
    static getHeight() {
        return WindowRemote.window.getSize()[1];
    }

    /**
     * @public
     *
     * Sets the size of the window.
     *
     * @param {number} width - New width for the window.
     * @param {number} height - New height for the window.
     */
    static setSize(width, height) {
        WindowRemote.window.setSize(width, height);
    }

    /**
     * @public
     *
     * Centers the application on the user's screen.
     */
    static center() {
        WindowRemote.window.center();
    }
}

module.exports = WindowRemote;

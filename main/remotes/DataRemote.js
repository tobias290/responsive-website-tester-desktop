const Remote = require("./Remote");

class DataRemote extends Remote {
    constructor(db) {
        super();
        this.db = db;
    }

    static get displayTypesDb() {
        return new DataRemote(DataRemote.getGlobal("displayTypesDb"));
    }

    static get displaysDb() {
        return new DataRemote(DataRemote.getGlobal("displaysDb"));
    }

    static connect(displayTypes, displays) {
        let displayTypesDb = new (require("nedb"))({filename: displayTypes, autoload: true});
        let displaysDb = new (require("nedb"))({filename: displays, autoload: true});

        global.displayTypesDb = displayTypesDb;
        global.displaysDb = displaysDb;

        displayTypesDb.find({}, (err, docs) => {
            if (docs.length === 0)
                displayTypesDb.insert(require("../defaults/defaultDisplayData"));
        });
    }

    findAll() {
        return new Promise((resolve, reject) => {
            this.db.find({}, (err, docs) => {
                if (err)
                    reject(err);

                resolve(docs);
            });
        });
    }

    insert(data) {
        return new Promise((resolve, reject) => {
            this.db.insert(data, (err, newDoc) => {
                if (err)
                    reject(err);

                resolve(newDoc);
            });
        });
    }

    remove(id) {
        return new Promise((resolve, reject) => {
            this.db.remove({_id: id}, (err, numRemoved) => {
                if (err)
                    reject(err);

                resolve(numRemoved);
            });
        });
    }
}

module.exports = DataRemote;
